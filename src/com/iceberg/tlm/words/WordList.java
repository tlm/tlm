package com.iceberg.tlm.words;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.iceberg.tlm.TLM;

public class WordList {

	private String dict;

	private TLM tlm;

	private Map<String, Word> words = new HashMap<String, Word>();

	public WordList(TLM tlm, String dict) {
		this.tlm = tlm;
		this.dict = dict;
	}

	public String getDict() {
		return dict;
	}

	public void setDict(String dict) {
		this.dict = dict;
	}

	public TLM getTlm() {
		return tlm;
	}

	public void setTlm(TLM tlm) {
		this.tlm = tlm;
	}

	public void add(Word word) {
		words.put(word.getOriginal(), word);
	}

	public Word get(String original) {
		return words.get(original);
	}

	public Collection<Word> getWords() {
		return words.values();
	}

}
