package com.iceberg.tlm.words;

import java.util.ArrayList;
import java.util.List;

import com.iceberg.tlm.TLM;

public class WordsModel {

	public TLM tlm;

	private List<WordList> wordLists = new ArrayList<WordList>();

	private WordsParser parser;

	public WordsModel(TLM tlm) {
		this.tlm = tlm;
		parser = new WordsParser(tlm);
		uploadAllInternalWordLists();
	}

	public TLM getTlm() {
		return tlm;
	}

	public void setTlm(TLM tlm) {
		this.tlm = tlm;
	}

	public void addWordList(WordList wordList) {
		wordLists.add(wordList);
	}

	public Word getWord(String original) {
		Word word = null;
		for (WordList wordList : wordLists) {
			word = wordList.get(original);
			if (word != null)
				return word;
		}
		return null;
	}

	public void uploadInternalWordList(String name) {
		addWordList(parser.getInternalWordList(name));
	}

	public void uploadAllInternalWordLists() {
		uploadInternalWordList("AlphabetAssociationWordsList.xml");
		/*
		 * NOT WORKS WITH ANDROID for (String file : Helper.listInternalFiles(
		 * WordsParser.INTERNAL_WORDS_FOLDER, "xml")) {
		 * uploadInternalWordList(file); }
		 */
	}
}
