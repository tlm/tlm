package com.iceberg.tlm.words;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.iceberg.tlm.Helper;
import com.iceberg.tlm.TLM;

public class WordsParser {

	public static final String XML_ID = "id";

	public static final String XML_DICT = "dict";

	public static final String XML_WORD = "word";

	public static final String XML_ORIGINAL = "original";

	public static final String XML_TRANSLATE = "translate";

	public static final String XML_PRONOUNCE = "pronounce";

	public static final String INTERNAL_WORDS_FOLDER = "words";

	private TLM tlm;

	public WordsParser(TLM tlm) {
		this.tlm = tlm;
	}

	public TLM getTLM() {
		return tlm;
	}

	public WordList getInternalWordList(String pathInInternalWordsFolder) {
		String string = Helper
				.getInternalResourceAsString(INTERNAL_WORDS_FOLDER
						+ File.separator + pathInInternalWordsFolder);
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(string));
			Document document = builder.parse(is);
			Element root = document.getDocumentElement();
			String listId = root.getAttribute(XML_DICT);
			NodeList wordsList = root.getChildNodes();
			WordList wordList = new WordList(getTLM(), listId);
			for (int i = 0; i < wordsList.getLength(); i++) {
				Node wordNode = wordsList.item(i);
				if (wordNode.getNodeName().equals(XML_WORD)) {
					NodeList wordOpts = wordNode.getChildNodes();
					int id = Integer.parseInt(wordNode.getAttributes()
							.getNamedItem(XML_ID).getNodeValue());
					String original = null;
					String translate = null;
					String pronounce = null;
					for (int j = 0; j < wordOpts.getLength(); j++) {
						Node opt = wordOpts.item(j);
						if (opt.getNodeName().equals(XML_ORIGINAL)) {
							original = opt.getFirstChild().getNodeValue();
						} else if (opt.getNodeName().equals(XML_TRANSLATE)) {
							translate = opt.getFirstChild().getNodeValue();
						} else if (opt.getNodeName().equals(XML_PRONOUNCE)) {
							translate = opt.getFirstChild().getNodeValue();
						}
					}
					wordList.add(new Word(id, getTLM(), original, translate,
							pronounce));
				}
			}

			return wordList;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
