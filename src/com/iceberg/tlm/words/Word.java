package com.iceberg.tlm.words;

import com.iceberg.tlm.TLM;

public class Word {

	private int id;

	private String original;

	private String translate;

	private String pronounce;

	private TLM tlm;

	public Word(int id, TLM tlm, String original, String translate,
			String prnounce) {
		this.id = id;
		this.tlm = tlm;
		this.original = original;
		this.translate = translate;
		this.pronounce = prnounce;
	}

	public int getId() {
		return id;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getTranslate() {
		return translate;
	}

	public void setTranslate(String translate) {
		this.translate = translate;
	}

	public String getPronounce() {
		return pronounce;
	}

	public TLM getTlm() {
		return tlm;
	}

	@Override
	public boolean equals(Object object) {
		return object instanceof Word
				&& getOriginal().equals(((Word) object).getOriginal());
	}

	@Override
	public String toString() {
		return "Word(\"" + getOriginal() + "\", \"" + getTranslate() + "\")";
	}

}
