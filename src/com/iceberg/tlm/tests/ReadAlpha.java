package com.iceberg.tlm.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class ReadAlpha {

	public static final String path = "/home/iceberg/alina/tlearn/m/env/expr/alpha/alpha";

	public static void main(String[] args) {

		String string = read();

		String original = null;
		int i = 1;
		for (String spliitedStr : string.split("\n")) {
			String[] splt = spliitedStr.split(" ");
			if (original == null) {
				original = splt[1];
			} else {
				String pronounce = splt[1];
				String shift = "\t";
				System.out.println(shift + "<word id=\"" + i + "\">");
				System.out.println(shift + shift + "<original>" +  original.trim() + "</original>");
				System.out.println(shift + shift + "<translate>"  + ""  + "</translate>");
				System.out.println(shift + shift + "<pronounce>" + pronounce.trim().toLowerCase()  + "</pronounce>");
				System.out.println(shift + "</word>");
				i++;
				original = null;
			}
		}

	}

	public static String read() {
		try {
			return readStream(getStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static InputStream getStream() {
		File file = new File(path);
		try {
			return new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String readStream(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder(512);
		Reader r = new InputStreamReader(is, "UTF-8");
		int c = 0;
		while ((c = r.read()) != -1) {
			sb.append((char) c);
		}
		return sb.toString();
	}

}
