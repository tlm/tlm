package com.iceberg.tlm.tests;

import com.iceberg.tlm.TLM;
import com.iceberg.tlm.cases.AlphabetCase;
import com.iceberg.tlm.cases.sets.AlphabetCasePackage;

public class AssociationTest {

	public static void main(String[] args) {
		TLM tlm = new TLM();
		AlphabetCasePackage casePackage = tlm.getAlphabet().getCasePackage();
		AlphabetCase tCase = (AlphabetCase) casePackage.getCases().get(0);
		tCase.next();
		System.out.println(tCase.getDirectString());
		// System.out.println(updateAssociatedWordString(tCase));
	}

	/*
	 * protected static String updateAssociatedWordString(AlphabetCase tCase) {
	 * Word word = tCase.getSymbol().getAssociation(); return " " + (word ==
	 * null ? "" : word.getOriginal()) + " "; }
	 */
}
