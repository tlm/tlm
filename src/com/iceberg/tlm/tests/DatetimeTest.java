package com.iceberg.tlm.tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DatetimeTest {

	public static void main(String[] args) {
		System.out.println(dtCurrentUTC().getTime());
		Date date=new Date(dtCurrentUTC().getTime());
		System.out.println(date);

/*		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(dtCurrentUTC().getTime());
		int mYear = calendar.get(Calendar.YEAR);
		int mMonth = calendar.get(Calendar.MONTH);
		int mDay = calendar.get(Calendar.DAY_OF_MONTH);*/
		
	}

	public static final String dbDateTimeFormat = "yyyy:MM:dd HH:mm:ss";

	public static SimpleDateFormat getFormatter(String zone) {
		SimpleDateFormat formatter = getDefaultFormatter();
		formatter.setTimeZone(TimeZone.getTimeZone(zone));
		return formatter;
	}

	public static String dtsCurrentUTC() {
		return getFormatter("UTC").format(System.currentTimeMillis());
	}

	public static SimpleDateFormat getDefaultFormatter() {
		return new SimpleDateFormat(dbDateTimeFormat, Locale.getDefault());
	}

	public static Date dtCurrentUTC() {
		try {
			return getDefaultFormatter().parse(dtsCurrentUTC());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
