package com.iceberg.tlm.tests;

import com.iceberg.tlm.TLM;
import com.iceberg.tlm.alphabet.Symbol;

public class SeqTest {

	public static void main(String[] args) {
		TLM tlm = new TLM();
		System.out.println(tlm.getAlphabet().getConsonants().length);
		Symbol[] symbols = new Symbol[tlm.getAlphabet().getConsonants().length];
		for (int i = 0; i < symbols.length; i++) {
			Symbol curSymbol = tlm.getAlphabet().getConsonants()[i];
			if (curSymbol.getIndex() > 0) {
				symbols[curSymbol.getIndex() - 1] = curSymbol;
			}
		}
		for (int i = 0; i < symbols.length; i++) {
			if (symbols[i] != null) {
				System.out.println(symbols[i].getSign() + " = "
						+ symbols[i].getIndex() + " = "
						+ symbols[i].getSoundInputStream());
			}
		}
	}

}
