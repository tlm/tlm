package com.iceberg.tlm.tests;

import java.io.File;

import com.iceberg.tlm.Helper;
import com.iceberg.tlm.TLM;
import com.iceberg.tlm.cases.AbstractCase;
import com.iceberg.tlm.cases.AbstractCasePackage;
import com.iceberg.tlm.cases.AlphabetCase;

public class Tests {

	public void Tests() {
	}

	public final String getResource(String res) {

		// Helper.listInternalFiles("/resources/words", "xml");

		res = File.separator + Helper.INTERNAL_RESOURCE_DIR + File.separator
				+ "words" + "/";
		// URL url = Helper.getResource(absPath);

		Object object = this.getClass().getResource(res);
		return object == null ? (res + ": NO") : (res + ": YES");
		/*
		 * final StringBuilder sb = new StringBuilder();
		 * 
		 * try { final String fmt = ("%s(%s%s)\n  %s\n");
		 * sb.append(String.format(fmt, "ClassLoader.getResource", "", res,
		 * this.getClass().getClassLoader().getResource(res)));
		 * sb.append(String.format(fmt, "Class.getResource", "/", res,
		 * this.getClass().getResource("/" + res)));
		 * 
		 * } catch (Throwable t) { StringWriter sw = new StringWriter();
		 * PrintWriter pw = new PrintWriter(sw);
		 * sb.append("\n\nGot Exception!!!!\n"); t.printStackTrace(pw);
		 * pw.flush(); sb.append(sw.toString()); }
		 * 
		 * return sb.toString();
		 */
	}

	public static void main(String[] args) {
		TLM tlm = new TLM();
		AbstractCasePackage casePackage = tlm.getAlphabet().getCasePackage();
		for (AbstractCase tCase : casePackage.getCases()) {
			if (tCase instanceof AlphabetCase) {
				AlphabetCase aCase = (AlphabetCase) tCase;
				System.out.println("\nCASE: "
						+ aCase.getClass().getSimpleName());
				for (int i = 0; i < 11; i++) {
					String directString = aCase.getDirectString();
					String assignString = aCase.getAssignedString();
					System.out.println(directString + " - " + assignString
							+ " - " + aCase.isRightResponse(assignString));
					aCase.next();
				}
			}
			if (tCase instanceof AlphabetCase) {
				AlphabetCase aCase = (AlphabetCase) tCase;
				System.out.println("\nCASE: "
						+ aCase.getClass().getSimpleName());
				for (int i = 0; i < 11; i++) {
					String directString = aCase.getDirectString();
					String assignString = aCase.getAssignedString();
					System.out.println(directString + " - " + assignString
							+ " - " + aCase.isRightResponse("p"));
					aCase.next();
				}
			}
		}
	}
}
