package com.iceberg.tlm;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.words.WordsModel;

public class TLM {

	public static final int LANG_RU = 0;

	public static final int LANG_EN = 1;

	public static final int PRONOUNCIATION_FORM_SHORT = 0;

	public static final int PRONOUNCIATION_FORM_LONG = 1;

	private int lang = LANG_RU;

	private int pronounciationForm = PRONOUNCIATION_FORM_LONG;

	private Alphabet alphabet = new Alphabet();

	private boolean isCompareFormSensitive = true;

	private boolean isCompareLangSensitive = true;

	private WordsModel wm = new WordsModel(this);

	public TLM() {

	}

	public WordsModel getWordModel() {
		return wm;
	}

	public boolean isCompareFormSensitive() {
		return isCompareFormSensitive;
	}

	public void setCompareFormSensitive(boolean isCompareFormSensitive) {
		this.isCompareFormSensitive = isCompareFormSensitive;
	}

	public boolean isCompareLangSensitive() {
		return isCompareLangSensitive;
	}

	public void setCompareLangSensitive(boolean isCompareLangSensitive) {
		this.isCompareLangSensitive = isCompareLangSensitive;
	}

	public Alphabet getAlphabet() {
		return alphabet;
	}

	public int getLang() {
		return lang;
	}

	public void setLang(int lang) {
		this.lang = lang;
	}

	public int getPronounciationForm() {
		return pronounciationForm;
	}

	public void setPronounciationForm(int pronounciationForm) {
		this.pronounciationForm = pronounciationForm;
	}

}
